/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import database.User;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author KiTTiWaT
 */
public class UserTableModel extends AbstractTableModel{
    ArrayList<User> userlist = new ArrayList();
    String[] columnName = {"#","login name","name","surname","type"};

    @Override
    public int getRowCount() {
        return userlist.size();
        
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User user = userlist.get(rowIndex);
        switch(columnIndex){
            case 0: return user.getUserId();
            case 1: return user.getLoginName();
            case 2: return user.getName();
            case 3: return user.getSername();
            case 4: return user.getTypeId()==0?"Admin":"User";
        }
        return "";
    }
    public void setData(ArrayList<User> userlist){
        this.userlist = userlist;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnName[column];
    }
    
    
}
